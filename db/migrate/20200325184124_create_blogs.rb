class CreateBlogs < ActiveRecord::Migration[6.0]
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :content
      t.datetime :published_at
      t.boolean :draft
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
    add_index :blogs, :published_at
    add_index :blogs, :draft
  end
end
